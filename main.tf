# main.tf

provider "aws" {
  region = "us-east-2"  # Change this to your desired region
}

# Create a VPC
resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true

  tags = {
    Name = "my-vpc"
  }
}

# Create subnets
resource "aws_subnet" "subnet_a" {
  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "us-east-2a"  # Change this to your desired availability zone
  map_public_ip_on_launch = false

  tags = {
    Name = "subnet-a"
  }
}


resource "aws_subnet" "subnet_b" {
  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block              = "10.0.2.0/24"
  availability_zone       = "us-east-2b"  # Change this to your desired availability zone
  map_public_ip_on_launch = false

  tags = {
    Name = "subnet-b"
  }
}

# Create security groups
resource "aws_security_group" "web_sg" {
  vpc_id = aws_vpc.my_vpc.id

  // Define inbound and outbound rules as needed
}

resource "aws_security_group" "db_sg" {
  vpc_id = aws_vpc.my_vpc.id

  // Define inbound and outbound rules as needed
}

# Create RDS instance
resource "aws_db_instance" "my_db" {
  identifier            = "my-db-instance"
  allocated_storage    = 20
  storage_type          = "gp2"
  engine               = "mysql"  # Change this based on your preferred database engine
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"  # Change this based on your requirements
  #name                 = "mydatabase"
  username             = "admin"
  password             = "admin"
  publicly_accessible  = false
  vpc_security_group_ids = [aws_security_group.db_sg.id]
}

# Create EC2 instance
resource "aws_instance" "web_instance" {
  ami           = "ami-xxxxxxxxxxxxxx"  # Specify the AMI ID for your desired OS
  instance_type = "t2.micro"  # Change this based on your requirements
  subnet_id     = aws_subnet.subnet_a.id
  vpc_security_group_ids = [aws_security_group.web_sg.id]

  // Add other necessary configurations
}

# Outputs
output "database_endpoint" {
  value = aws_db_instance.my_db.endpoint
}
